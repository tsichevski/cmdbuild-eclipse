# cmdbuild-eclipse

Here you will find the instruction how to import the [cmdbuild](http://www.cmdbuild.org/en) projects to Eclipse to make it possible to build and debug that projects.

The has been was written for and tested on the release 2.5.1 of _cmdbuild_.

## Non-eclipse-related part

On that stage we will (mostly) follow the [instruction by tecnoteca](https://bitbucket.org/tecnoteca/cmdbuild/wiki/Home).

### Load & Install dependencies

1. Load [dependencies](https://bitbucket.org/tecnoteca/cmdbuild-dependencies/downloads/cmdbuild-dependencies-2.0-SNAPSHOT-20120416.zip);
2. Run `install-artifacts.sh` (or `install-artifacts.cmd` on Windows)
3. Build & Install the `shark-ws-plain` module manually:

```
pushd cmdbuild-dependencies/net/sourceforge/sharkwf/shark-ws-plain/
mvn -Dmaven.test.skip install
```

### Download & prepare the package

Clone the repo on Bitbucket (note, we must use the `cmdbuild-main` as the root catalog name):

```
git clone https://bitbucket.org/tecnoteca/cmdbuild cmdbuild-main
```

Switch to the 2.5.1 release commit (do not use HEAD, it compiles, but I did not manage to make it work!):

```
cd cmdbuild-main/
git checkout 2.5.1
```
Edit `distribution/pom.xml`, comment out both `old-shark-*` modules (2 lines);
Edit `distribution/cmdbuild/pom.xml`, comment-out `cmdbuild-distribution-old-shark-overlay` dependency (6 lines);

### Build with maven

At this point the repository shall be compilable with maven:

```
mvn install
```

The compilation takes about 10 minutes on my computer.

_Note_: due to some broken dependencies, you should not omit test when building for the first time. Next time build with:

```
mvn -Dmaven.test.skip install
```

### Install the result to tomcat webapps:

Install the resulting WAR to tomcat `webapps/` catalog:

```
cp cmdbuild/target/cmdbuild-2.5.1.war ~/tomcat/webapps/cmdbuild.war
```

Wait a while while tomcat deploys the WAR, then go to http://localhost:8080/cmdbuild

## Eclipse-related part

To make the artefacts Eclipse-compatible (M2Eclipse), you shal make some ajustments to the sources.

This instruction was written for Eclipse Photon Release (4.8.0).

### Rename subdirectories

_Problem_: some subprojects are located  in the project tree in the directories with same names, which confuses _m2ecliple_ when it deduces project names for eclipse.
_Solution_: rename directories to remove the ambiguity.

Do the renamings:

```
git mv bim/core bim/bim-core
git mv dms/core dms/dms-core
git mv scheduler/core scheduler/scheduler-core
git mv scheduler/quartz scheduler/scheduler-quartz
git mv auth/default auth/auth-default
git mv auth/core auth/auth-core
git mv dao/core dao/dao-core
git mv shark/legacy-extensions shark/shark-legacy-extensions
git mv shark/server shark/shark-server
git mv shark/client shark/shark-client
git mv shark/commons shark/shark-commons
git mv shark/extensions shark/shark-extensions
git mv services/rest services/services-rest
git mv services/sync/core services/sync/sync-core
git mv services/sync/internal services/sync/sync-internal
git mv services/sync/sql services/sync/sync-sql
```

Edit the root `pom.xml`, adjust modules correspondingly:

```
<module>auth/auth-core</module>
<module>auth/auth-default</module>
<module>bim/bim-core</module>
<module>dao/dao-core</module>
<module>dms/dms-core</module>
<module>scheduler/scheduler-core</module>
<module>scheduler/scheduler-quartz</module>
<module>services/services-rest</module>
<module>shark/shark-client</module>
<module>services/sync/sync-core</module>
<module>services/sync/sync-internal</module>
<module>services/sync/sync-sql</module>
<module>shark/shark-commons</module>
<module>shark/shark-extensions</module>
<module>shark/shark-legacy-extensions</module>
<module>shark/shark-server</module>
```

Edit the `core/pom.xml`: add the `pluginExecution` to the `build`:

```
<pluginManagement>
  <plugins>
    <plugin>
  <groupId>org.eclipse.m2e</groupId>
  <artifactId>lifecycle-mapping</artifactId>
  <version>1.0.0</version>
  <configuration>
    <lifecycleMappingMetadata>
      <pluginExecutions>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>org.antlr</groupId>
                    <artifactId>antlr3-maven-plugin</artifactId>
                    <versionRange>[${antlr.version},)</versionRange>
                    <goals>
                      <goal>antlr</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <execute>
                      <runOnIncremental>false</runOnIncremental>
                    </execute >
                  </action>
                </pluginExecution>
      </pluginExecutions>
    </lifecycleMappingMetadata>
  </configuration>
    </plugin>
  </plugins>
</pluginManagement>
```

This will save us from the following _M2Eclipse_ problem:

```
Plugin execution not covered by lifecycle configuration: org.antlr:antlr3-maven-plugin:3.5.2:antlr (execution: default, phase: generate-sources)
```

Edit the `parent/pom.xml` file, change the line 376:

```
<version>2.2.4</version>
```
to
```
<versionRange>[2.2.4,)</versionRange>
```

### Generate sources

_Note_: if you had built projects with Maven previously, you may probably omit this step.

Automatically generate some sources with Maven:

```
mvn generate-sources
```

### Import to Eclipse

1. _File_->_Open project from File System_ ;
2. Select the cmdbuild repository root. Eclipse should detect 32 subprojects;
3. Wait while Eclipse downloads sources and javadocs;

### Fix project configurations

At this moment, you will see multiple errors in almost all subprojects. Now you need to fix the project configurations created automatically by _M2Eclipse_.

#### Fix the _core_ module

Add the entry for the auto-generated sources to the `core/.classpath` file:

```
<classpathentry kind="src" output="target/classes" path="target/generated-sources/antlr3">
    <attributes>
        <attribute name="optional" value="true"/>
        <attribute name="maven.pomderived" value="true"/>
    </attributes>
</classpathentry>
```

#### Fix the _cmdbf_ module

Add the entry for the auto-generated sources to the `cmdbf/.classpath` file:

```
<classpathentry kind="src" output="target/classes" path="target/generated/cxf">
	<attributes>
		<attribute name="optional" value="true"/>
		<attribute name="maven.pomderived" value="true"/>
	</attributes>
</classpathentry>
```

#### Fix the _ws-client_ module

Add the entry for the auto-generated sources to the `ws-client/.classpath` file:

```
<classpathentry kind="src" output="target/classes" path="target/generated/cxf">
	<attributes>
		<attribute name="optional" value="true"/>
		<attribute name="maven.pomderived" value="true"/>
	</attributes>
</classpathentry>
```


### Rebuild all projects

In Eclipse select menu item _Project_->_Clean..._->_Clean all projects_.

Now you should see only the JSP- and XML-related errors.
